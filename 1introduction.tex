\chapter{Introduction}
\label{introduction}

\lstset{language=R}

%�

%introduction
%- motivation
%- R
%- digest-algos
%

\section{Motivation}
The programming language R is widely used for statistics, bioinformatics and many other research fields. R's reference implementation, GNU R, uses bytecode interpretation, which incurs an overhead over native execution in terms of run time and memory consumption \cite{MorandatHillOsvaldEtAl2012}. Therefore many computationally expensive parts of R and third-party packages are written in other programming languages like C. The digest package is one example for such a package that uses C code for the actual digest calculations and R code just for a lightweight wrapper mainly for performance reasons.

The goal of this work was to implement the core-functionality of the digest package in pure R and evaluate the performance in comparison to the traditional C implementation. For improved performance the alternative R implementation FastR was used, which aims to optimize the execution of R code dynamically at run-time to reach the performance of traditional C code. The pure R implementation should provide a reference package for analyzing and optimizing the dynamic compilation and execution of R code in FastR.


\section{R}
%language
% -object oriented
% -functional
% -dynamically typed
% - data centric?
%compiler is oss
% - interpreter
% - user interface

The term R typically refers to both the programming language and its official and most widely used implementation. To distinguish between the reference implementation and alternative implementations like FastR, pqR or Renjin the term \textit{GNU R} is used for the former while the term \textit{R} is used for the language.

Providing a language for data anlysis and graphics has been the goal for designing R, with syntax and features similar to S and semantics similar to Scheme \cite{Ihaka1996}. The language combines concepts from both object-oriented and functional programming languages although it does not enforce either of them \cite{Chambers2014}. Handling datasets easily and efficiently is a central task of the language, which is based on vector operations. R's dynamic type system gives the programmer a lot of freedom regarding the structure of data.

The reference implementation GNU R is freely available and open source software under the GNU General Public License (GPL). A big software ecosystem with development tools (like RStudio) and a large number of extensions have contributed to the success of R. Today  R is very popular among scientists for analyzing/exploring/visualizing data, performing statistical/mathematical calculations and many other tasks \cite{Tippmann2014}. 

GNU R presents itself to the user as interactive console application waiting for R code input and subsequently evaluating the command and printing the results before starting over again. It is implemented as an interpreter, but there are extensions available that add a Just-in-time (JIT) compiler or allow to compile R code to bytecode \cite{Tierney2016}. Memory management is mainly done by the interpreter (and not by the user) using a garbage collector.




\subsection{Data-Types}
While R defines well-known data types from other programming languages like integer or character they are quite different in their use and internal representation. The most fundamental difference to classic programming languages like C is that the basic data types are stored as vectors \cite{Chambers2008}. For example a single integer value is actually an integer vector of length one. The following list describes the most important properties of basic R data types (classes).
\begin{description}
\item[numeric]
is the default type for numbers in R. The object type is \texttt{double} and allows to store floating point numbers with double precision. A number (vector) can be converted to numeric using the \texttt{as.numeric(x)} function.
\item[integer]
is a 32-bit signed integer numeric type. Numeric (double) values can be converted to integer values with the \texttt{as.integer(x)} function. Constants can be specified to be integer constants by appending an uppercase \texttt{L}, e.g. \texttt{23L}. Some integrated functions like the sequence operator \texttt{from:to} return integer values. The values can range from -2147483647 to +2147483647. Note that -2147483648 (\texttt{0x80000000}) is not a valid integer number as it represents the special value \texttt{NA}.
\item[character]
is a character string. Despite its name it does not store single characters, but strings. R uses the platform's native encoding by default, but also can handle specific character encodings like UTF-8 or latin1. The length of a string (in characters or bytes) can be determined using the \texttt{nchars(s)} function.
\item[raw]
is a 8-bit unsigned integer numeric type. It stores single bytes in the range from 0 to 255 (\texttt{0x00} to \texttt{0xff}). The stored value is usually displayed in hexadecimal format e.g. by the \texttt{print} function. A value can be converted to \texttt{raw} using the \texttt{as.raw(x)} function and an empty \texttt{raw} vector of length \textit{n} can be created using \texttt{raw(n)}.
\end{description}
As R is a functional language there are also datatypes for functions. R has some more types for basic types (like complex number) and other language features that are not discussed here.

For unknown or missing values R uses the special \texttt{NA} (not available) value. It is defined for the all vector types except \texttt{raw}. To check for \texttt{NA} one can use the \texttt{is.na(o)} function. Calculations involving \texttt{NA} typically result in the value \texttt{NA} again.
\\
For empty objects R defines the \texttt{NULL} object. In contrast to \texttt{NA} it is a type of its own. The \texttt{is.null(o)} can be used to check whether an object \textit{o} is \texttt{NULL}. A special property is that assigning \texttt{NULL} to a list element removes it \cite{Brown2010}.


\subsection{Vectors and Lists}
%% Actually all the basic data types introduced in the previous section are vectors \cite{Chambers2008}. For example a single integer value is actually an integer vector of length one. That is a fundamental difference in data handling compared to traditional programming languages like C. 
Vectors are a basic data type in R. The programming language defines atomic and generic vectors - the former are called vectors and the latter are called lists.
\\
A \textit{vector} is a list (an array) of objects of the same type in R. The typical way of creating a vector is by using the \texttt{c()} (concatenate) function. In case the function is called with arguments of different types they are converted to a common type. For example the following call results in a character vector:
\begin{lstlisting}
> c(1,2L,"3",4)
[1] "1" "2" "3" "4"
\end{lstlisting}
A \textit{list} can store an (one-dimensional) array of objects of different types. As basic values are vectors in R a list is typically an array of vectors. It can be created by using the \texttt{list()} function.

There are two indexing methods for vectors and lists available in R. The first one uses the syntax \texttt{object[index]} where \textit{object} specifies a vector or list object and \textit{index} specifies an indexing vector. The \texttt{[]} indexing function returns a vector/list containing all the elements with the specified indices. Elements can be either indexed numerically or by name in case such a name was defined. Numeric indexing starts with one. Negative indices exclude the element at the absolute index position from the beginning. A named index can be defined like this for example: \texttt{c(name=value, another=other)}.
\\
The other indexing method uses the syntax \texttt{object[[index]]} where \textit{object} again specifies a vector or list object but \textit{index} specifies only a single indexing value or name (a vector of length one). In contrast to the other indexing method \texttt{[[]]} always returns a single element (vector). This is commonly used for extracting a single element (vector) from a list where \texttt{[]} would return a list with a single element (vector) inside.
\\
An alternative syntax for named indices in lists allows to shorten the expression \texttt{obj[[name]]} to \texttt{obj\$name}. This syntax is not available for vectors although they also provide named indices.

Elements can be added to a list by re-creating it using e.g. \texttt{c(list, newvalue)} or by simply storing data at an yet unused index using \texttt{list[[newindex]] <- value}. If numeric indices are used any intermediate indices will be set to the value \texttt{NA}, so there is always a continuous sequence of numeric indices.


\subsection{Expressions}

\begin{description}

\item{\textbf{Operators}} \\
The infix notation is used to describe expressions in R. Here is an (incomplete) list of common operators in the R language, listed in their order of precedence with the highest precedence first:
\begin{itemize}[nosep]
%%\setlength\itemsep{-1.5em}
\item Indexing \texttt{[]}, \texttt{[[]]}
\item Exponentiation \texttt{\^{}}
\item Unary Plus/Minus \texttt{-}, \texttt{+}
\item Sequence operator \texttt{:}
\item Special operators \texttt{\%\%} (modulo), \texttt{\%/\%} (integer division)
\item Multiply/Divide \texttt{*}, \texttt{/}
\item Add/Subtract \texttt{-}, \texttt{+}
\item Comparision \texttt{<}, \texttt{>}, \texttt{<=}, \texttt{>=}, \texttt{==}, \texttt{!=}
\item Negation \texttt{!}
\item And \texttt{\&}, \texttt{\&\&}
\item Or \texttt{|}, \texttt{||}
\item Assignment \texttt{->}, \texttt{->{}>}
\item Assignment \texttt{<-}, \texttt{<{}<-}
\item Assignment \texttt{=}
\end{itemize}
Additionally to the default assignment operator \texttt{<-} R also knows other assignment operators including \texttt{=} and \texttt{<{}<-} which differ in their precedence and scoping rules. The \texttt{=} operator is often used for named function arguments in function calls.

The sequence operator \texttt{x:y} is used to create simple ascending or descending sequences from a start value \textit{x} to an (inclusive) end value \textit{y} with a step size of 1. The operator returns an integer vector except a non-integer start value is given. It is often used in loops and for indexing. R supports assignments to sub-ranges of vectors/lists.
\begin{lstlisting}
a <- 1:5    		# a: [1] 1 2 3 4 5
b <- 5:1    		# b: [1] 5 4 3 2 1
d <- 1.3:4  		# d: [1] 1.3 2.3 3.3
a[2:4] <- b[4:6]	# a: [1]  1  2  1 NA  5
\end{lstlisting}


\item{\textbf{Control Structures}} \\
R defines control structures like if-clauses and loops, but there are some differences to their counterparts in traditional imperative programming languages, mostly due to the functional nature of R.

The \textbf{if-clause} of R returns the evaluation result of either statement A or B depending on the evaluation result of \textit{condition}. Thus it is comparable to the ternary operator \texttt{?:} in C-like programming languages. Multiple if-clauses can be nested to form else-if blocks. The else clause is optional.
\begin{lstlisting}
if (condition) statementA else statementB
if (tmp >= 2147483648) (tmp-4294967296) else (tmp)
\end{lstlisting}
Usually there is a single statement per line in R, but one can also put several statements in a single line by separating them with a semicolon(\texttt{;}).  Multiple statements can be wrapped in curly braces to form blocks, which can span over multiple lines.

The usage of loops is discouraged in R because vector operations should be used instead wherever this is possible. That allows to speed up the computation (e.g. using parallel computing). Nevertheless there exist 3 types of loop structures: \texttt{for}, \texttt{while} and \texttt{repeat}.
\\
The \textbf{for-loop} is used to iterate over a vector/list. In each iteration \textit{var} is assigned the value of the current vector item and \textit{statement} is evaluated (which may also be a block). 
\\
The \textbf{while-loop} evaluates a \textit{condition} at the beginning of each iteration and evaluates \textit{statement} in case the condition resulted in \texttt{TRUE}.
\\
The \textbf{repeat-loop} evaluates the block of statements forever and must be interrupted by the \texttt{break} command in order to avoid an endless loop.
\begin{lstlisting}
for (var in vector) statement
for(i in 1:10) print(i)

while (condition) statement
i <- 0
while (i < 10) { i <- i + 1; print(i) }

repeat { statements ... }
i <- 0
repeat {
	i <- i + 1L
	if (i > 10) break
	print(i)
}
\end{lstlisting}

Finally the \textbf{switch-statement} selects an item from a (named) list by its numeric index or by its name as string. In case no list item was found switch returns \texttt{NULL}.
\begin{lstlisting}
switch(selector, list)
switch(n %% 4L + 1L, "four", "one", "two", "three")
switch("a", a=2, b=4, d=7)		# returns 2
\end{lstlisting}


\end{description}

 
\subsection{Functions}
Functions are a fundamental part of R to provide its functional nature. As in other functional programming languages functions are first-class language elements in R which means they can be assigned to variables, passed as parameter or returned from other functions. In R functions are objects. A definition, implementation and call of a simple function to calculate the sum of two numbers may look like this:
\begin{lstlisting}
add2 <- function(x, y) {
	x + y
}
add2(24, 7)
\end{lstlisting}
A R function consists of three main components: the body, the formals and the environment which are explained in greater detail below \cite{Wickham2014}.
\begin{description}
\item[formals]
are the formal function arguments in the function definition. There are no type restrictions or descriptions for the arguments as R is a dynamic language, so values of any type may be passed to functions and the function has to check explicitly, if necessary. In the example above \texttt{(x, y)} are the formals and would allow the addition of any types (for example vectors or matrices). It is possible to specify default values for specific arguments using the syntax \texttt{(x=default1, y=default2)}, which makes them optional. Otherwise all the arguments have to be specified.
\item[body]
contains the executed statements for the function, which may be another function call, an expression or a sequence of imperative instructions (among others). The evaluation result of the last executed statement of a function is the return value of the function. Alternatively a value can be returned explicitly by using the \texttt{return} function.
\item[environment]
is comparable to the visibility scope of a function in a traditional programming language like C (but R's scoping rules are somewhat different). It defines the currently available names including their values and references a parent environment. The default environment is the global environment, in which the \textit{add2} function above would be defined. A function defines its own environment which has the calling environment as parent. Variable names in the body are resolved by traversing the environment hierarchy from the function environment upwards to the root (empty environment).
\end{description}

A function call in R looks like in C with the arguments enclosed in parantheses. While the syntax is very similar the semantics are a bit different as R uses lazy evaluation for arguments. That means that expressions passed as argument will not be evaluated until their value is used for the first time. This is fundamentally different from C or Java where method arguments are always evaluated before the method call whether they are used or not.
R additionally supports named arguments which allows to specify parameters by name when calling a function: \texttt{add2(y=2, x=1)}. This also allows arguments to be specified in any order.



\subsection{Objects and Classes}
R is an object-oriented language, thus everything is treated as an object. The type of an object \textit{o} can be retrieved using the \texttt{typeof(o)} function. Note that the datatypes presented in the previous section are classes, whose class name can be retrieved using the \texttt{class(o)} function for an object \textit{o}.

Every object in R has a list of attributes for storing additional information about an object. Attributes are typically used to store meta data like named array indices or matrix dimensions \cite{Burns2012}. The list can be retrieved using the \texttt{attributes(o)} function for an object \textit{o}. Single attributes are accessed using the \texttt{attr(o, name)} function for both reading and writing the value of an attribute \textit{name} of an object \textit{o}.

Classes are somewhat special in the R language compared to traditional object-oriented programming languages as there exist several different concepts for classes, including S3 classes and S4 classes. An object can be defined as instance of a certain S3 class by setting the \textit{class} attribute to the class name. This can be done using \texttt{attr(o, "class") <- "classname"} or \texttt{class(o) <- "classname"} for an object \textit{o}. The central concept for S3 classes are \textit{generic functions}, that are called first and dispatch to a specific implementation for the corresponding class depending on the passed argument. The implementation has to define a generic function and specialized functions as needed. The generic function calls  \texttt{UseMethod(fname, o)} to dispatch to a specialized function for the class of object \textit{o} named \textit{fname}. The specialized function uses a special naming convention, which appends the classname to the generic function name, separated with a dot.

S4 classes are defined using the \texttt{setClass()} function, which allows to specify \textit{slots} (member variables), a \textit{prototype} (initializer), a validation function and a list of superclasses (parameter \textit{contains}), among others. Class methods can be defined using the functions \texttt{setGeneric()} and \texttt{setMethod()}. In contrast to other object-oriented languages like Java class methods are tied to function names in R instead of class objects.


\subsection{Packages}
Packages allow to extend the functionality of R in a very easy and user friendly way. A package combines source code, data, tests, documentation, metadata and other files in a well structured format inside a single compressed archive file. To use an existing package it has to be installed first. During that process the contents of the package are compiled (if necessary), verified and finally copied to the local R installation's library directory. A package can be loaded or unloaded at any time during runtime of an R script.

The largest, oldest and most comprehensive collection of R packages is CRAN, the Comprehensive R Archive Network, with almost 10000 packages as of december 2016\footnote{\url{https://cran.r-project.org/}}. The number of packages increases nearly exponentially \cite{Hornik2012}. Other popular services for R package development and distribution are \textit{GitHub}, \textit{R-Forge} and \textit{BioConductor} while the former two are mainly used for package development \cite{Decan2015}.

GNU R comes with a lot of tools that aim to simplify package handling and development tasks including creation, installation, testing and building \cite{Wickham2016}. The listing below contains function calls to install and load a package from inside R. A package is loaded using the \texttt{library(package)} or \texttt{require(package)} functions where \textit{package} specifies the package name. By default CRAN is used to install new packages and additional arguments are necessary to install a package from a local source as in the example below.
\begin{lstlisting}
install.packages("package")
install.packages("/path/to/pkg.tar.gz", repos=NULL, type="source")
library("package")
\end{lstlisting}
Alternatively packages can be installed from the system's command line directly as shown in the following  example. This example also includes commands to check a package for validity and build a package from a package source directory.
\begin{lstlisting}[language=bash]
$ R CMD CHECK package
$ R CMD BUILD package
$ R CMD INSTALL /path/to/package.tar.gz 
\end{lstlisting}

A R package is built from a directory with a special standardized structure. The function \texttt{package.skeleton()} can be used to create a skeleton structure for a new package. Modifying a package or looking into an existing package is very often as simple as uncompressing the package with file archival software. The most important files and folders inside a package are the following ones:
\begin{itemize}[nosep]
\item A \texttt{DESCRIPTION} file is the only file that is required to be present and contains a lot of important meta data for the package. It contains the package name, title, version number, information about its author(s) and maintainer(s), publication date, a short description, dependencies on specific R versions or other packages, license information and optionally a lot of further package information.
\item The \texttt{NAMESPACE} file specifies the symbol names that will be publically visible (outside of a package). By default all the symbols in a package are private (only visible within the package) and need to be \textit{exported} explicitly to make them available from outside. This avoids cluttering the global namespace with lots of symbols when loading a larger amount of packages.
\item A \texttt{R} directory contains all the R source code of the package. If there are components written in other programming languages than R those sources are located in the \texttt{src} directory. Foreign-language components are typically written in C, C++ or Fortran \cite{Leisch2008}.
\item The documentation is located in the \texttt{man} directory. It uses a custom \texttt{.rd} file format (R documentation) that is a markup language very similar to \LaTeX{} \cite{RCT2016}.
\item The \texttt{data} directory allows to include datasets in a package. Multiple formats are supported including a binary format, comma-separated-values and R code \cite{Chambers2008}. R provides the functions \texttt{save(x, file="x.rda")} and \texttt{load(file="x.rda")} to save a data object \textit{x} in a binary format to a file \textit{x.rda} or load it from that file. Although it is also possible to include data as R code this method is typically not suitable for large datasets because of the large overhead caused by the file format.
\end{itemize}






\section{FastR}

% fastr
% graal/truffle

% open source, license
% fastr architecture

% why R is slow?
% - symbol/variable look-up
% - linked lists used e.g. for function arguments

% GNU-R: source -(parse)-> parse tree/AST -(interpret)-> Exe
% AST -(compile/link)-> machine code -> Exe

% Self-optimizing AST interpreter; AST rewrites, inlining
% JVM based (GC, JIT-compilation)

% Optimizations:
% - FastR: function calls, symbol lookup
% - Truffle: node rewriting
% - Graal: partial evaluation, machine code compilation


FastR is a R interpreter written in Java. As the name suggests it aims to execute R code faster than GNU R while staying compatible with existing R code and packages written for GNU R. In its current state it already supports many packages including the \textit{digest} package one one hand, but lacks support for certain popular packages relying on GNU R internals like \textit{ggplot2} on the other hand.

\subsection{Architecture}

FastR is built upon Truffle and Graal which provide an infrastructure to execute an Abstract Syntax Tree (AST) on the Java Virtual Machine (JVM). The parser generator ANTLR is used for generating the R parser code in FastR. During parsing a  Truffle AST is built and operations like symbol lookup and function calls are optimized among others \cite{Stadler2016}. Especially symbol lookup is typically a slow operation in R because of the complex scoping rules which require a search in the environment hierarchy quite often. Truffe provides means for replacing AST nodes by specialized ones, called \textit{node rewriting}. That allows optimizations for specific data types or   specific symbol lookup results among others \cite{Wuerthinger2013}. The AST is further optimized using partial evaluation with support from the Graal JIT compiler. Partial evaluation can simplify the AST nodes based on certain assumed preconditions: e.g omitting code paths that are rarely entered and inlining methods in the compiled version based on assumptions about future code execution derived from information about past executions. Finally machine code is generated by the Graal JIT compiler.



\subsection{Availability}

FastR, Truffle and Graal are open source software. The source code is available from \url{https://github.com/graalvm/fastr}, precompiled binaries are available too. Currently FastR is only supported on Linux and MacOS X operating systems but not on Microsoft Windows.







\section{Digest Algorithms}
Digest algorithms take data of arbitrary length as input and calculate a fixed-length hash value with certain properties. They are used for error detection and integrity checking in data transmission, data storage and cryptography, among others.

\subsection{MD5}
The message-digest algorithm MD5 as defined in RFC 1321 calculates a 128-bit (32-byte) fingerprint from input data of any length. It was designed for an efficient execution on typical 32-bit hardware \cite{Rivest1992}.
Originally it was considered to be suitable for cryptographic purposes but over the years multiple attacks on MD5 have been published so that MD5 can be broken within a very short time nowadays \cite{WangYu2005}.
The fast execution and simple implementation probably have contributed to the wide spreading of the algorithm. It it still used in many non-security-critical areas nowadays (e.g. file integrity checks).

\subsection{CRC}
The cyclic redundancy check algorithm was developed to detect errors in data transmission. An important parameter for the algorithm is the so-called generator polynomial, which represents the divisor of a polynomial division in the algorithm and directly influences the length of the result. There exist multiple versions of the algorithm and many proposals for generator polynomials. In this paper the CRC-32 variant as defined in the IEEE 802.3 standard with the following generator polynomial $G(x)$ is used \cite{IEEE2012}.
\[
G(x) = x^{32} + x^{26} + x^{23} + x^{22} + x^{16} + x^{12} + x^{11} + x^{10} + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
\]
The degree of the polynomial is 32  which results in a generated checksum of 32 bits (4 bytes).


\subsection{SHA}
The algorithms of the Secure Hash Algorithm (SHA) family are specified in FIPS-180-4 \cite{NIST2015}. They are considered to be cryptographic hash functions, that means they aim to fulfill the following properties \cite{Rogaway2004}:
\begin{itemize}
\item Pre-image resistance: it is practically infeasible to find an input value $x$ for a given hash value $y$ so that $h(x) = y$.
\item Second pre-image resistance: it is practically infeasible to find an input value $x'$ different from a given value $x$ so that $h(x) = h(x')$ and $x' \neq x$ (for a given $x$).
\item Collision resistance: it is practically infeasible to find input values $x$ and $x'$ so that $h(x) = h(x')$ and $x' \neq x$ (for any $x, x'$).
\end{itemize}
As of now there exist three versions called SHA-1, SHA-2 and SHA-3. While the first version used a fixed hash value length the later versions are available in multiple variants for different hash value lengths. That is SHA-224, SHA-256, SHA-384 and SHA-512 (the value specifies the number of bits) of the SHA-2 family \cite{Vadhera2014}. SHA-3 typically also uses one of the hash value lengths above but additionally allows a variable hash value length.
The hash function design of the first two versions is based on Merkle-Damgard construction, which is also the principle behind MD5. As there are some known generic attacks against that type of hash functions  SHA-3 is based on a different design: sponge construction \cite{Sobti2012}. In 2017 a collision for SHA-1 could be computed successfully (violating the \textit{Collision resistance} property), which renders SHA-1 unsuitable for cryptographic purposes \cite{Stevens2017}.
 


\section{Encryption Algorithms}
Encryption algorithms transform unencrypted \textit{plaintext} into encrypted \textit{ciphertext} using an \textit{encryption key}. Usually the output is of the same length as the input data. To reverse the encryption process the decryption algorithm transforms ciphertext back to plaintext. It is often very similar to the encryption algorithm.
Encryption algorithms are classed as either symmetric or asymmetric. While the former use the same key for both encryption and decryption the latter use a so-called \textit{public key} for encryption and a (different) \textit{private key} for decryption.
Encryption is used to protect data from unauthorized access. Although encryption renders data unusable (without knowing the key) it is not suitable to protect data/communications against tampering.


\subsection{AES}
The Advanced Encryption Standard (AES) as specified in FIPS-197, also called Rijndael-algorithm, is a symmetric encryption algorithm with a key length of 128, 192 or 256 bits \cite{NIST2001}. Rijndael was designed to be not only secure but also simple and efficient and so it has been chosen from a set of candidates in a competition to be the AES in the year 2000 \cite{Daemen2002}. Since then it has been adopted by a large number of users and is still considered to be secure as of today although some attacks have been published that slightly reduce complexity \cite{Kaminsky2010}.

Rijndael is a block cipher, which means that the encryption is applied to blocks of a fixed size each. The process for a single block consists of several iterations applying transformations to the block. Typical simple transformations are substitution and permutation as used by the AES, among others. To encrypt data larger than the block size the encryption process has to be executed repeatedly in a secure manner. So-called \textit{modes of operation} describe how to use the block cipher to encrypt multiple blocks. Common modes are \textit{Electronic Codebook} (ECB), \textit{Cipher Block Chaining} (CBC) or \textit{Counter} (CTR).

