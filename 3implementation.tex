\chapter{Implementation}
\label{implementation}

%implementation
%- how is binaryop included?
%- etc.
%

%�


% r limitations:
\section{Limitations of R data types}
All the digest and encryption algorithms implemented in the digest package rely on binary operations on data. Languages with low-level access to hardware are benefitial for performing such operations in a fast and efficient manner. In contrast languages like R, which give only high-level access to the data, often lack specific low-level operations. So those operations have to be implemented as a combination of other available operations which often results in a slow implementation.


\subsection{Numeric data types}
% int does not support binary operations
% limited value range (N/A, no 64-bit datatype, no unsigned types)
The default \texttt{numeric} data type in R is a double-precision floating-point data type and thus not suitable for fast integer arithmetics required for the digest algorithms. More suitable is the \texttt{integer} data type, a 32-bit signed integer type. The functions \texttt{bitwNot}, \texttt{bitwAnd}, \texttt{bitwOr}, \texttt{bitwXor}, \texttt{bitwShiftL} and \texttt{bitwShiftR} provide bitwise manipulations of 32-bit unsigned integers. The bit-rotation operator can be implemented using a combination of shift operators and \texttt{bitwOr}.

However there is the special \texttt{NA} value that is part of the 32-bit integer range. Operations on \texttt{NA} always result in \texttt{NA} or give an error message. This renders the \texttt{integer} type unusable for digest calculation as-is. As a possible workaround one could implement custom functions for bitwise and arithmetic operations that check specifically for NA and treat this value in a special way.

Another downside of the \texttt{integer} data type is its restriction to the signed integer range. There is no unsigned integer. Additionally there is no integer type with a higher precision than 32-bits. Some algorithms (like SHA-512) require 64-bit integers which are not available natively in R. There are packages that provide such a 64-bit integer type (such as \texttt{bit64}\footnote{\url{https://cran.r-project.org/web/packages/bit64/}}) but those do not support bitwise operations and suffer from the \texttt{NA} issue as well.



\subsection{Raw data type}
% raw does not support arithmetic operations
An alternative to numeric data types is the \texttt{raw} data type, which represents an unsigned byte vector. As such it supports most bitwise operations natively using the \texttt{\&} (and), \texttt{|} (or), \textbf{!} (not) and \texttt{xor} operators. Shift operators are provided by the \texttt{rawShift} method. However, this method operates on single bytes only and does not shift from one byte to another. A custom shift method combining the bytewise shifts with a bitwise or solves this issue. Similarily bitwise rotation can be implemented.

The issue seen with \texttt{rawShift} occurs also for all arithmetic operators: overflow/underflow does not propagate to the next byte. This means that all needed arithmetic operators require a custom implementation as well. Luckily addition (and substraction) are the only arithmetic operations needed for implementing the selected digest algorithms and AES does not even use any of them.

Values larger than 32-bits are no problem with \texttt{raw} vectors as they can be of virtually any size. Additionally the vector nature makes extracting bytes, words, double-words or quad-words from a byte vector simply a matter of indexing a subrange. In contrast to integers \texttt{raw} values can use the whole byte range from 0 to 255 without special treatment for \texttt{NA}.


\subsection{Arithmetic properties}
% different overflow behaviour
Arithmetic operations in C and similar languages behave as implemented in hardware with a restriction to a certain number of bits. From a mathematical point of view the operations are performed in a residue class ring modulo $ 2^n $ where \textit{n} is the number of bits. That means that they operate within a limited range of numbers and values can overflow (the most significant bits are dropped). The digest algorithms rely on that behaviour.

In R however, calculations result in \texttt{NA} when they overflow (instead of starting again from zero) as shown in the example below. $ 2^{31}-1 $ is the largest positive number that the \texttt{integer} type in R can represent.
\begin{lstlisting}
> as.integer(2^31-1)+1L
[1] NA
Warning message:
In as.integer(2^31 - 1) + 1L : NAs produced by integer overflow
\end{lstlisting}
Thus the overflow behaviour of C has to be simulated in R - for example by subtracting the maximum value of the used data type manually if necessary. Of course that calculation is only possible with a larger data type like \texttt{numeric}. When using \texttt{raw} vectors the overflow behaviour does not cause additional problems as they do not support arithmetic operators anyway and overflow handling is already part of the operator implementation.



\subsection{Vectorization}
% linear hash algorithms: md5, .. do not support parallelization
% designed to run efficiently on actual hardware / low level programming languages
As already mentioned in the introduction section R does not have simple value data types but there are only vectors of values. In general R relies a lot on vectorization to become reasonable fast. That means applying operations on a vector of data instead of applying them on single values. The use of iterative programming structures like \texttt{for} loops and \texttt{if} conditions is discouraged and usually slow in R \cite{Burns2012}. Instead code should be vectorized.

However all the implemented digest algorithms work in an linear, iterative way and do not support parallelization and vectorization. They were designed to run efficiently on actual (simple) hardware and thus are easier to implement in low-level languages like C than in high-level languages like R. None of the implemented algorithms is designed for parallel execution, but there do exist such algorithms: for example MD6 is one of them \cite{Rivest2008}.





\section{Bitwise and Arithmetic operators}
% operations:
% - addition of raw vectors with overflow
% - rotate right/left
% - shift
% - trunc32 / makeInt32
For the R implementation \texttt{raw} vector was choosen as the default data type for binary data as it does not suffer from the \texttt{NA} issue and allows to extract any number of bytes easily with vector indexing. This section explains the implementation of important operators in more detail.


\subsection{Endianness conversion}
% - bswap -> see endianness
%   endianness: little/big endian
There are basically two different approaches how to store a binary number of multiple bytes in memory: \textit{Little-Endian} takes the approach of storing the least significant byte at the lowest address and the most significant byte at the highest address while \textit{Big-Endian} does it the other way round. Most of today's computers are little-endian systems. Still, common digest algorithms are specified for big-endian systems with the SHA-family of algorithms as an example. MD5 on the other hand is described for little-endian systems in the official specification.

As both shall be implemented a method is needed that converts a value from one byte orientation to the other one. Luckily that is a quite straightforward operation and only requires to swap the bytes. Processors based on the x86 instruction set provide the \texttt{bswap} instruction for this operation. In R the conversion is very simple too if a \texttt{raw} vector was used as all that needs to be done is indexing the bytes in reverse order as shown below.

\begin{lstlisting}
as.raw(c(1,2,3,4))[4:1]
[1] 04 03 02 01
\end{lstlisting}


\subsection{Addition with overflow}
Arithmetic operators are not supported for \texttt{raw} vectors in R as mentioned in the previous section. Thus a custom R implementation first converts the \texttt{raw} vectors into \texttt{integer} vectors, applies the addition operator and corrects overflow and finally converts the result back into a \texttt{raw} vector. A schematic implementation for 16-bit little-endian numbers is shown in the code listing below. The Java FastR implementation similarily converts the byte array to an integer value, performs the addition and converts that value back to a byte array as C/C++ features like pointers and re-interpreting casts are not available in Java either.
\begin{lstlisting}
rawAdd16LE <- function(x, y) {
  tmp <- as.integer(x) + as.integer(y)
  tmp[2] <- tmp[2] + (tmp[1]%/%256L)
  tmp[1] <- tmp[1]%%256L
  tmp[2] <- tmp[2]%%256L
  as.raw(tmp)
}
\end{lstlisting}


\subsection{Bit rotation and bit shifts}
Bit shifts for bytes are implemented by R already. To extend this functionality for words and double-words two shift operations - one shifting $ n \bmod 8 $ bits to the left (where \textit{n} is the number of bits) and another one shifting $ 8-(n \bmod 8) $ to the right - are combined. A further case distinction selects the resulting byte indices for shifts with $ n > 8 $. Bit rotation is very similar to bit shifting and just slightly differs in the combination of the two byte-shifted vectors.
The FastR implementation simply converts the byte array to an integer value, performs the shift and converts the value back to a byte array. Bit rotation is compound of two shifts as a bit rotation operation is not available in Java.


\subsection{Conversions}
Sometimes it is necessary or at least convenient to convert between the \texttt{numeric}/\texttt{integer} and and the \texttt{raw} vector representation of numbers, for example to perform more difficult calculations, like sine and multiplication, which are not implemented for raw vectors. This is done using the \texttt{writeBin} and \texttt{readBin} functions provided by R for writing resp. reading binary data to/from \texttt{raw} vectors. Beside input and output the functions take the \textit{size} in bytes and the \textit{endian}ness as arguments.
To convert a \texttt{numeric} representation of an integer to the \texttt{integer} type the value is first constrained to 32-bits ($ value \bmod 2^{32} $) and then converted to a signed integer by subtracting $ 2^{32} $ if the value is greater or equal to $ 2^{31} $.



\subsection{Optimizations}
% optimization:
% integers instead of floating-point constants
% don't use varargs
% use global null-raw-vector instances
While the algorithm implementations certainly are not optimized and follow the reference implementation or descriptions of the respective standard some small-scale optimizations have been applied to reduce the costs for the various custom operator implementations. The following optimizations improved the performance notably:
\begin{itemize}[itemsep=-5pt]
%\itemsep-1em
\item Use \texttt{integer} constants instead of \texttt{numeric} constants by adding the \texttt{L} suffix to  integer constants.
\item Re-use objects. For example the implementation uses a global instance for the value zero as 32-bit \texttt{raw} vector.
\item Pre-allocate vectors if the size is known in advance instead of constantly increasing its size during creation. 
\item Avoid using functions with a variable argument count for frequently called methods as they are slower than defining separate functions for every necessary argument count.
\item Use vector operations instead of iterative loops
\end{itemize}


\section{FastR extensions}
Some important binary or arithmetical operations needed for efficient digest calculation are missing in GNU R as discussed in the previous sections. The idea was to implement those operations in FastR natively and analzye the speed gain compared to a runtime where that functionality is not available.

\subsection{Truffle node implementation}

The new functions are implemented as built-in nodes, which are located in the Java package \textit{com.oracle.truffle.r.nodes.builtin} containing implementations for basic methods like \texttt{min}, \texttt{max} or \texttt{range}. A sample implementation for such a node is sketched below. It implements the addition of two integer values without special \texttt{NA} handling.

\begin{lstlisting}[language=java,breaklines=true]
@RBuiltin(name = "fastr.rawi.rawAdd32LE", kind = PRIMITIVE, parameterNames = {"x", "y"}, behavior = PURE)
public abstract class RawIAdd32LE extends RBuiltinNode {
    static {
        Casts casts = new Casts(RawIAdd32LE.class);
        casts.arg("x").asIntegerVector();
        casts.arg("y").asIntegerVector();
    }
    @Specialization
    protected Integer rawAdd32LE(Integer x, Integer y) {
        return x + y;
    }
    @Specialization
    protected RIntVector rawAdd32LE(RIntVector x, RIntVector y) {
    	...
    }
}
\end{lstlisting}

The \texttt{RBuiltin} annotation describes the R interface and basic properties of the node. Its \texttt{name} will the the function name used to call the method from R and the arguments are specified in the \texttt{parameterNames} field. \texttt{kind} specifies how the method is called and is always \texttt{PRIMITIVE} for all \textit{raw}/\textit{rawi} operations, meaning that the functions are called directly by name. \texttt{behaviour} declares the method as pure function, which means that is does not depend on state information and always returns the same results for the same input.

\textit{Casts} describe the expected data types for the individual function arguments and how to convert them. Implementations of the function are marked with the \texttt{Specialization} annotation. Their formal parameters are compared to the actually passed arguments in the call and the matching implementation is called. In the example above there are two specializations, one for adding two integer values and another one for adding two integer vectors. The R datatypes are automatically converted to their Java counterparts.

The node has to be added to a package to be used. So we add it to the \texttt{base} package, which is always available without the need to be loaded explicitly. The implementation is shown below. It refers to the \texttt{RawIAdd32LENodeGen} class which is automatically generated from the node class by a truffle annotation processor.
\begin{lstlisting}[language=java,breaklines=true]
add(RawIAdd32LE.class, RawIAdd32LENodeGen::create);
\end{lstlisting}


\subsection{Limitations of Java}

While Java shares more semantical properties with C than with R there are still some differences to plain C. The probably biggest difference is that Java does not support pointers. They allow e.g. to easily reference 32-bit integer values inside a larger block of bytes. In Java however, single byte values have to be composed to a 32-bit value using sevaral binary operations. Especially the conversion between \texttt{Integer} and \texttt{byte[]} (in both directions) is not as easy as in C. The class \texttt{java.nio.ByteBuffer} provides some helper methods for those operations.
